package com.nova.springcore;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.nova.beans.AppConfig;
import com.nova.beans.AppConfig2;
import com.nova.beans.Ciudad;
import com.nova.beans.IEquipo;
import com.nova.beans.Jugador;
import com.nova.beans.Mundo;
import com.nova.beans.Persona;

public class App {

	public static void main(String[] args) {
		//TRABAJANDO CON LA CLASE JUGADOR
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("com/nova/xml/beans.xml");
		Jugador jug  = (Jugador) applicationContext.getBean("messi");
		System.out.println(jug.getNombre()+" "+jug.getNumero()+" "+jug.getEquipo().mostrar());
		((ConfigurableApplicationContext)applicationContext).close();
		//TRABAJANDO CON LA CLASE PERSONA
		//si nuestro bean est configurado con la propiedad de lazy'initi = "true"; entonces
		//podremos manejar manejar la inicializacion de las instancias; esto siempre y cuando
		//el scope este en singleton
		//ApplicationContext applicationContext = new ClassPathXmlApplicationContext("com/nova/xml/beans.xml");
		//Persona persona = (Persona)applicationContext.getBean("persona");
		/*
		String ciudades = "";
		for(Ciudad ciudad: persona.getPais().getCiudades()){
			ciudades += ciudad.getNombre()+" ";
		}*/
		//System.out.println(persona.getId() + " " + persona.getNombre() + " " + persona.getApodo()+" "+persona.getPais().getNombre() + " "+persona.getPais().getNombre()+" "+persona.getCiudad().getNombre());
		//((ConfigurableApplicationContext)applicationContext).close();
		/*TRABAJANDO CON LA CLASE MUNDO
		//creamos la clase ApplicationContext, y llamamos al beans.xml
		//si el beans.xml se encentra en la misma carpeta que la clase entonces no es necesario especificar la ruta
		//llamamos al beans.xml
		//ApplicationContext applicationContext = new ClassPathXmlApplicationContext("com/nova/xml/beans.xml");
		
		//llamamos a la anotacion
		//podemos agregar varias anotaciones en dos formas
		//Primera Forma
		//ApplicationContext applicationContext = new AnnotationConfigApplicationContext(AppConfig.class,AppConfig2.class);
		
		//Segunda Forma
		AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
		applicationContext.register(AppConfig.class);
		applicationContext.register(AppConfig2.class);
		applicationContext.refresh();
		
		//llamamos a la clase
		Mundo mundo = applicationContext.getBean(Mundo.class);//primera forma
		//Mundo mundo= (Mundo)applicationContext.getBean("mundo"); 
		System.out.println(mundo.getSaludo());
		//liberamos el recurso de applicationContext
		((ConfigurableApplicationContext)applicationContext).close();
		*/
	}

}
