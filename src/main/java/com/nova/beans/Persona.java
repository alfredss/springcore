package com.nova.beans;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class Persona implements InitializingBean, DisposableBean{
	private int id;
	private String nombre;
	private String apodo;
	private Pais pais;
	private Ciudad ciudad;
	
	public Persona(int id, String nombre, String apodo){
		this.id = id;
		this.nombre = nombre;
		this.apodo = apodo;
	}
	
	public Persona(int id){
		this.id = id;
	}
	
	public Persona(String nombre){
		this.nombre = nombre;
	}
	/*haciendo uso de anotaciones para la gestion del ciclo de vida de un Bean
	@PostConstruct
	public void init(){
		System.out.println("Inicializamos el Bean con anotaciones");
	}
	@PreDestroy
	public void destroy(){
		System.out.println("Apunto de Destruir el Bean con anotaciones");
	}*/
	
	public Persona(){
		
	}

	public Ciudad getCiudad() {
		return ciudad;
	}

	public void setCiudad(Ciudad ciudad) {
		this.ciudad = ciudad;
	}

	public int getId() {
		return id;
	}

	public String getNombre() {
		return nombre;
	}

	public String getApodo() {
		return apodo;
	}

	public Pais getPais() {
		return pais;
	}

	public void setPais(Pais pais) {
		this.pais = pais;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public void setApodo(String apodo) {
		this.apodo = apodo;
	}
	
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		System.out.println("Inicializamos el Bean con implements");
	}
	
	public void destroy() throws Exception {
		// TODO Auto-generated method stub
		System.out.println("Apunto de Destruir el Bean con implements");
	}

	
	

}
