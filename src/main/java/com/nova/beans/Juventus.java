package com.nova.beans;

public class Juventus implements IEquipo{
	private String nombre;
	public Juventus(){
		
	}
	public Juventus(String nombre){
		this.nombre=nombre;
	}
	public String mostrar() {
		return "Juventus";
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
}
