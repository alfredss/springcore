package com.nova.beans;

import java.util.List;

public class Pais {
	private String nombre;
	//private List<Ciudad> ciudades;
	
	public void init(){
		System.out.println("Inicializamos el Bean");
	}
	
	public void destroy(){
		System.out.println("Apunto de Destruir el Bean");
	}
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/*
	public List<Ciudad> getCiudades() {
		return ciudades;
	}

	public void setCiudades(List<Ciudad> ciudades) {
		this.ciudades = ciudades;
	}
	*/
	
}
